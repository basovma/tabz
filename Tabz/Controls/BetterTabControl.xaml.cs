﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Tabz.Controls
{
    /// <summary>
    /// Interaction logic for BetterTabControl.xaml
    /// </summary>
    public partial class BetterTabControl : UserControl
    {
        public BetterTabControl()
        {
            InitializeComponent();

            tabCounter = tabControl.Items.Count + 1;
        }
        private int tabCounter = 0;

        private Point _dragStart;
        private bool _isDragging;

        private void TabItem_PreviewMouseLeftButtonDown(object sender, MouseEventArgs e)
        {
            _dragStart = e.GetPosition(null);
        }

        private void TabItem_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            var position = e.GetPosition(null);

            if (e.LeftButton == MouseButtonState.Pressed && !_isDragging)
            {

                if (Math.Abs(position.X - _dragStart.X) > SystemParameters.MinimumHorizontalDragDistance ||
                Math.Abs(position.Y - _dragStart.Y) > SystemParameters.MinimumVerticalDragDistance)
                {
                    _isDragging = true;

                    var tabItem = e.Source as TabItem;

                    if (tabItem == null)
                        return;

                    DragDrop.DoDragDrop(tabItem, tabItem, DragDropEffects.All);

                    _isDragging = false;
                }

            }
        }


        private void TabItem_Drop(object sender, DragEventArgs e)
        {
            var tabItemTarget = e.Source as TabItem;

            var tabItemSource = e.Data.GetData(typeof(TabItem)) as TabItem;

            if (!tabItemTarget.Equals(tabItemSource))
            {
                var tabControl = tabItemTarget.Parent as TabControl;
                int sourceIndex = tabControl.Items.IndexOf(tabItemSource);
                int targetIndex = tabControl.Items.IndexOf(tabItemTarget);

                tabControl.Items.Remove(tabItemSource);
                tabControl.Items.Insert(targetIndex, tabItemSource);

                tabControl.Items.Remove(tabItemTarget);
                tabControl.Items.Insert(sourceIndex, tabItemTarget);

                tabControl.SelectedItem = tabItemSource;
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            var item = new TabItem();

            item.Header = "TabItem" + tabCounter;

            item.Content = new char[80 * 1024];

            this.tabControl.Items.Add(item);

            this.tabControl.SelectedItem = item;

            tabCounter++;
        }

        public void button_TabCloseClick(object sender, RoutedEventArgs e)
        {
            var item = (TabItem)((e.Source as Button).TemplatedParent as ContentPresenter).TemplatedParent;

            this.tabControl.Items.Remove(item);

            if (item.Content is IDisposable)
            {
                (item.Content as IDisposable).Dispose();
            }

            BindingOperations.ClearAllBindings(item);

            item.DataContext = null;
            item.Header = null;
            item.Content = null;
        }
    }
}
